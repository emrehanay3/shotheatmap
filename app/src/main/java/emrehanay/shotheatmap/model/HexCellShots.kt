package emrehanay.shotheatmap.model

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import emrehanay.shotheatmap.R
import java.util.*

class HexCellShots(var columnIndex: Int, var rowIndex: Int) {
    companion object {
        const val DENSITY_VERY_LOW = 0
        const val DENSITY_LOW = 1
        const val DENSITY_HIGH = 2
        const val DENSITY_VERY_HIGH = 3

        const val DENSITY_MAX_LEVEL = DENSITY_VERY_HIGH
    }

    val shots: ArrayList<Shot> = ArrayList()

    private fun successRate(): Int {
        var success = 0
        var fail = 0
        for (shot in shots) {
            if (shot.isSuccess) {
                success++
            } else {
                fail++
            }
        }
        if (success + fail == 0) {
            return 0
        }
        return 100 * success / (success + fail)
    }

    fun getColorBySuccessRatio(context: Context?): Int {
        val ratio = successRate()
        return when {
            ratio >= 0 && ratio < 12.5 -> ContextCompat.getColor(context!!, R.color.heatMapLevel1)
            ratio >= 12.5 && ratio < 25 -> ContextCompat.getColor(context!!, R.color.heatMapLevel2)
            ratio >= 25 && ratio < 37.5 -> ContextCompat.getColor(context!!, R.color.heatMapLevel3)
            ratio >= 37.5 && ratio < 50 -> ContextCompat.getColor(context!!, R.color.heatMapLevel4)
            ratio >= 50 && ratio < 62.5 -> ContextCompat.getColor(context!!, R.color.heatMapLevel5)
            ratio >= 62.5 && ratio < 75 -> ContextCompat.getColor(context!!, R.color.heatMapLevel6)
            ratio >= 75 && ratio < 87.5 -> ContextCompat.getColor(context!!, R.color.heatMapLevel7)
            ratio >= 87.5 && ratio <= 100 -> ContextCompat.getColor(
                context!!,
                R.color.heatMapLevel8
            )
            else -> Color.TRANSPARENT
        }
    }

    fun getDensity(avg: Int): Int {
        return when {
            shots.size < avg * .5 -> DENSITY_VERY_LOW
            shots.size < avg -> DENSITY_LOW
            shots.size < avg * 1.5 -> DENSITY_HIGH
            else -> DENSITY_VERY_HIGH
        }
    }

}