package emrehanay.shotheatmap.model

class Pair<K, V>(var key: K, var value: V) {
    override fun toString(): String {
        return key.toString() + "=" + value
    }
}