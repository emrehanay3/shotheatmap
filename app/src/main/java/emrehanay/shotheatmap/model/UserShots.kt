package emrehanay.shotheatmap.model

import android.content.Context
import com.fasterxml.jackson.annotation.JsonProperty
import emrehanay.shotheatmap.R
import java.io.Serializable

class UserShots : Serializable {

    @JsonProperty("name")
    val name: String? = null

    @JsonProperty("surname")
    val surname: String? = null

    @JsonProperty("shots")
    val shots: List<Shot>? = null

    fun getPlayerName(context: Context, index: Int): String {
        var playerName = ""
        if (name != null) playerName += "$name "
        if (surname != null) playerName += surname
        return (context.getString(R.string.player_title_s, index + 1) + " " + playerName).trim()
    }


}