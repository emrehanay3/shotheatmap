package emrehanay.shotheatmap.model;

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

class Shot : Serializable {

    @JsonProperty("point")
    var point: Int? = null

    @JsonProperty("segment")
    var segment: String? = null

    @JsonProperty("_id")
    var id: String? = null

    @JsonProperty("InOut")
    var inOut: Boolean? = null

    @JsonProperty("ShotPosX")
    var shotPosX: Float? = null

    @JsonProperty("ShotPosY")
    var shotPosY: Float? = null

    // deserialize
    constructor() {}
    constructor(shotPosX: Float?, shotPosY: Float?) {
        this.shotPosX = shotPosX
        this.shotPosY = shotPosY
    }

    val isSuccess: Boolean
        get() = java.lang.Boolean.TRUE == inOut

    override fun toString(): String {
        return "Shot(point=$point, segment=$segment, id=$id, inOut=$inOut, shotPosX=$shotPosX, shotPosY=$shotPosY)"
    }
}
