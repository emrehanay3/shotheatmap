package emrehanay.shotheatmap.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import emrehanay.shotheatmap.model.UserShots
import java.util.*

class UsersShotViewModel : ViewModel() {
    private var liveData: MutableLiveData<List<UserShots>>? = null

    fun getUserShotsListMutableLiveData(): MutableLiveData<List<UserShots>> {
        if (liveData == null) {
            liveData = MutableLiveData()
            liveData!!.value = ArrayList()
        }
        return liveData!!
    }
}