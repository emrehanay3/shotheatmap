package emrehanay.shotheatmap.service.model

import com.fasterxml.jackson.annotation.JsonProperty
import emrehanay.shotheatmap.model.UserShots
import java.io.Serializable

class ShotsResponse : Serializable {

    @JsonProperty("data")
    var data: List<UserShots>? = null
}