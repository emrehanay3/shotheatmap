package emrehanay.shotheatmap.service

import emrehanay.shotheatmap.service.model.ShotsResponse
import retrofit2.Call
import retrofit2.http.GET

interface IApiService {
    @GET("/shots")
    fun shots(): Call<ShotsResponse?>
}