package emrehanay.shotheatmap.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import emrehanay.shotheatmap.BuildConfig
import emrehanay.shotheatmap.service.model.ShotsResponse
import okhttp3.OkHttpClient
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

class RestService {
    private fun creteApiService(): IApiService {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        val client = builder.build()

        val objectMapper =
            ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(client)
            .build()

        return retrofit.create(IApiService::class.java)
    }

    fun shots(callback: Callback<ShotsResponse?>) {
        creteApiService().shots().enqueue(callback)
    }
}