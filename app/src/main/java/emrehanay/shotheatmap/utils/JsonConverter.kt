package emrehanay.shotheatmap.utils

import android.util.Log
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

object JsonConverter {
    private const val s = "JsonConverter"
    fun <T> fromJson(json: String?, c: Class<T>?): T? {
        val objectMapper =
            ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return try {
            objectMapper.readValue(json, c)
        } catch (e: IOException) {
            Log.e(s, e.message, e)
            null
        }
    }

    fun <T> fromJson(jsonPacket: String?, type: TypeReference<T>?): T? {
        val objectMapper =
            ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return try {
            objectMapper.readValue(jsonPacket, type)
        } catch (e: Exception) {
            Log.e(s, e.message, e)
            null
        }
    }

    fun toJson(src: Any?): String? {
        val objectMapper = ObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        return try {
            objectMapper.writeValueAsString(src)
        } catch (e: JsonProcessingException) {
            Log.e(s, e.message, e)
            null
        }
    }
}