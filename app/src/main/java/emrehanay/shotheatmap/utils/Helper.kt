package emrehanay.shotheatmap.utils

import android.content.Context
import android.util.Log
import emrehanay.shotheatmap.R
import java.io.ByteArrayOutputStream
import java.io.IOException

object Helper {
    var COURT_WIDTH = 15f
    var COURT_HEIGHT = 15f / 1.06
    var COURT_Y_OFFSET = 1.2f
    fun loadJSONFromAsset(context: Context): String {
        val inputStream = context.resources.openRawResource(R.raw.shots)
        val outputStream = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var len: Int
        try {
            while (inputStream.read(buffer).also { len = it } != -1) {
                outputStream.write(buffer, 0, len)
            }
            outputStream.close()
            inputStream.close()
        } catch (e: IOException) {
            Log.d("Helper", e.message, e);
        }
        return outputStream.toString()
    }
}