package emrehanay.shotheatmap

import android.content.Context
import android.graphics.PorterDuff
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import emrehanay.shotheatmap.model.HexCellShots
import emrehanay.shotheatmap.model.Pair
import emrehanay.shotheatmap.model.Shot
import emrehanay.shotheatmap.utils.Helper
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set
import kotlin.math.abs

class HexagonMap(private val context: Context) {
    private val s = "HexagonMap"

    private var secondMarginTop = 0
    private var secondMarginLeft = 0
    private var hexWidth = 0
    private var hexHeight = 0
    private var hexMarginLeft = 0
    private var hexMarginTop = 0
    private var columnCount = 0
    private var rowCount = 0

    private var imageViewMap =
        HashMap<String, Pair<ImageView, Int>>()

    // map data source
    private val cells: ArrayList<HexCellShots> = ArrayList<HexCellShots>()
    var successShots: Int = 0
    var failShots: Int = 0
    private var avgShotOnOneCell: Int = 0


    init {
        var hexSize = context.resources.getDimension(R.dimen.hexSize).toInt()
        hexSize = (hexSize * 1.2f).toInt()
        hexHeight = hexSize
        hexWidth = hexSize
        secondMarginTop = -1 * hexSize / 4
        secondMarginLeft = hexSize / 2
    }

    fun init(parent: ViewGroup, parentWidth: Int, parentHeight: Int) {

        // calculate column row sizes
        columnCount = parentWidth / hexWidth
        rowCount = parentHeight / (hexHeight + secondMarginTop)

        val width = parent.width * Helper.COURT_HEIGHT / Helper.COURT_WIDTH

        // fit to parent
        val parentLayoutParams =
            FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, width.toInt())
        parent.layoutParams = parentLayoutParams

        // create grid layout
        for (i in 0 until rowCount) {
            val row = createRow(i)
            for (j in 0 until columnCount) {
                val imageView = createColumn()
                row.addView(imageView)
                imageViewMap["c" + j + "r" + i] = Pair(imageView, 0)
            }
            parent.addView(row)
        }
    }

    private fun createRow(i: Int): ViewGroup {
        val row = LinearLayout(context)
        row.orientation = LinearLayout.HORIZONTAL

        val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        layoutParams.setMargins(
            if (i % 2 != 0) secondMarginLeft else 0,
            if (i == 0) 0 else secondMarginTop, 0, 0
        )

        row.layoutParams = layoutParams
        return row
    }

    private fun createColumn(): ImageView {
        val imageView = ImageView(context)
        val imageViewParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(hexWidth, hexHeight)
        imageViewParams.setMargins(hexMarginLeft, hexMarginTop, 0, 0)
        imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.hexagon))
        // imageView.alpha = .5f
        imageViewParams.setMargins(0, 0, 0, 0)
        imageView.layoutParams = imageViewParams
        imageView.visibility = View.INVISIBLE
        return imageView
    }

    fun updateMap(shots: List<Shot>) {
        Log.d(s, "called update map shots size is ${shots.size}")
        // clear data
        cells.clear()
        successShots = 0
        failShots = 0

        imageViewMap.forEach { (_, pair) ->
            pair.key.visibility = View.INVISIBLE
            pair.value = 0
        }

        for (shot in shots) {

            if (shot.shotPosX == null || shot.shotPosY == null || "-1" == shot.segment) {
                Log.d(s, "skip shot $shot")
                continue
            }

            // calculate shot on imageView position
            var x = shot.shotPosX!!
            var y = shot.shotPosY!!
            val xOffset = Helper.COURT_WIDTH / 2f
            val yOffset = Helper.COURT_Y_OFFSET
            x += xOffset
            y -= yOffset
            var columnIndex = (columnCount * x / Helper.COURT_WIDTH).toInt()
            var rowIndex = (rowCount * y / Helper.COURT_HEIGHT).toInt()

            if (columnIndex >= columnCount) columnIndex = columnCount - 1
            if (rowIndex >= rowCount) rowIndex = rowCount - 1

            columnIndex = abs(columnIndex)
            rowIndex = abs(rowIndex)

            Log.d(
                s,
                "shot calculated ${shot.shotPosX}, ${shot.shotPosY} convert to $x, $y mapped to ${columnIndex},${rowIndex} id ${shot.id} segment ${shot.segment}"
            )

            // add map data source
            // find cell by columnIndex and rowIndex
            var hexCellShot: HexCellShots? = null
            for (cell in cells) {
                if (cell.columnIndex == columnIndex && cell.rowIndex == rowIndex) {
                    hexCellShot = cell
                    break
                }
            }
            if (hexCellShot == null) {
                val element = HexCellShots(columnIndex, rowIndex)
                element.shots.add(shot)
                cells.add(element)
            } else {
                hexCellShot.shots.add(shot)
            }
        }

        // calculate density, success and fail shots
        for (cell in cells) {
            Log.d(s, "${cell.columnIndex} ${cell.rowIndex} ${cell.shots.size}")
            for (shot in cell.shots) {
                if (shot.isSuccess) successShots++
                else failShots++
            }
        }

        avgShotOnOneCell = (successShots + failShots) / cells.size
        Log.d(s, "avg shot on one cell $avgShotOnOneCell")
        Log.d(s, "success shots $successShots")
        Log.d(s, "fail shots $failShots")

        cells.forEach {
            val imageViewCount = imageViewMap["c" + it.columnIndex + "r" + it.rowIndex]
            if (imageViewCount != null) {

                val imageView = imageViewCount.key
                imageView.setColorFilter(
                    it.getColorBySuccessRatio(context),
                    PorterDuff.Mode.SRC_IN
                )
                imageView.visibility = View.VISIBLE
                val density = it.getDensity(avgShotOnOneCell)
                // max high density 0 padding
                // high 1k padding
                // low 2k padding
                // very low 3k padding
                val k = HexCellShots.DENSITY_MAX_LEVEL - density
                val padding = hexWidth * 1 / 8 * k
                imageView.setPadding(padding, padding, padding, padding)
                Log.d(s, "density $density padding $padding shot size ${it.shots.size}")
            }
        }
    }
}