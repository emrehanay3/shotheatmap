package emrehanay.shotheatmap

import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProviders
import emrehanay.shotheatmap.model.UserShots
import emrehanay.shotheatmap.service.RestService
import emrehanay.shotheatmap.service.model.ShotsResponse
import emrehanay.shotheatmap.utils.Helper
import emrehanay.shotheatmap.utils.JsonConverter
import emrehanay.shotheatmap.viewmodel.UsersShotViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_horizontal_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MainActivity : AppCompatActivity() {

    private var hexagonMap: HexagonMap? = null
    private var userShotsViewModel: UsersShotViewModel? = null

    // data-source
    private var selectedPlayerIndex: Int? = null
    private var playerUserShotList: List<UserShots> = ArrayList<UserShots>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        hexagonMap = HexagonMap(this)
        initActivity()
    }

    private fun initActivity() {
        userShotsViewModel = ViewModelProviders.of(this).get(UsersShotViewModel::class.java)
        userShotsViewModel?.getUserShotsListMutableLiveData()!!.observe(this) { source ->
            if (source != null) {
                playerUserShotList = source
                val userNames = ArrayList<String>()
                var index = 0
                source.forEach {
                    userNames.add(it.getPlayerName(this, index++))
                }

                val arrayAdapter =
                    ArrayAdapter(this, android.R.layout.simple_spinner_item, userNames)
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                rootView!!.spinner!!.adapter = arrayAdapter

                if (source.isNotEmpty() && selectedPlayerIndex == null) {
                    selectedPlayer(0)
                }
            }
        }

        rootView!!.spinner!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long
            ) {
                if (i < playerUserShotList.size) {
                    selectedPlayer(i)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        rootView!!.imageViewPerson?.setOnClickListener { selectPlayer() }
        rootView!!.textViewTitle?.setOnClickListener { selectPlayer() }
        rootView!!.imageViewPlayers?.setOnClickListener { selectPlayer() }

        // ui is ready?
        rootView!!.frameLayout?.viewTreeObserver!!.addOnGlobalLayoutListener(object :
            OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                rootView!!.frameLayout!!.viewTreeObserver.removeOnGlobalLayoutListener(this)

                // resize heat map if bigger than rootView
                val activityPadding = resources.getDimension(R.dimen.activity_paddingTop)
                    .toInt() + resources.getDimension(R.dimen.activity_paddingBottom).toInt()

                val heightLimit = containerLayout!!.height - activityPadding * 2
                if (rootView!!.frameLayout!!.height > heightLimit) {
                    val parentLayoutParams =
                        ConstraintLayout.LayoutParams(heightLimit, heightLimit)
                    rootView!!.frameLayout!!.layoutParams = parentLayoutParams
                }

                // hexagonMap, hexs initialize
                hexagonMap!!.init(
                    rootView!!.layoutGrid!!,
                    rootView!!.frameLayout!!.width,
                    rootView!!.frameLayout!!.height
                )

                // UI ready, get stats of players
                getPlayerUserShots()

            }
        })
    }

    /*private fun nineDotTest() {
         val shots = ArrayList<Shot>()
         shots.add(Shot(-7.5f, -7.0f))
         shots.add(Shot(-7.5f, 0f))
         shots.add(Shot(0f, -7.0f))
         shots.add(Shot(0f, 0f))
         shots.add(Shot(0f, 7f))
         shots.add(Shot(7.5f, 0f))
         shots.add(Shot(7.5f, 7.0f))
         shots.add(Shot(-7.5f, 7.0f))
         shots.add(Shot(7.5f, -7.0f))

         hexagonMap!!.updateMap(shots)
     }*/

    private fun selectPlayer() {
        rootView!!.spinner?.performClick()
    }

    private fun getPlayerUserShots() {

        selectedPlayerIndex = null
        if (BuildConfig.OFFLINE_WORK) {
            val jsonMockData = Helper.loadJSONFromAsset(this)
            val shotResponse = JsonConverter.fromJson(jsonMockData, ShotsResponse::class.java)
            if (shotResponse?.data != null) {
                userShotsViewModel!!.getUserShotsListMutableLiveData().value =
                    shotResponse.data!!
            }
        } else {
            val restService = RestService()
            restService.shots(object : Callback<ShotsResponse?> {
                override fun onResponse(
                    call: Call<ShotsResponse?>,
                    response: Response<ShotsResponse?>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody?.data != null) {
                            userShotsViewModel!!.getUserShotsListMutableLiveData().value =
                                responseBody.data
                            return
                        }
                    }
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.unknown_server_error_fetching_players_stats),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onFailure(call: Call<ShotsResponse?>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "${getString(R.string.unknown_server_error_fetching_players_stats)}\n\n${t.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
    }

    private fun selectedPlayer(index: Int) {
        selectedPlayerIndex = index
        if (selectedPlayerIndex != null) {
            val userShots = playerUserShotList[selectedPlayerIndex!!]

            // Update UI Components
            rootView!!.textViewTitle?.text = userShots.getPlayerName(this, index)
            hexagonMap!!.updateMap(userShots.shots!!)

            val total = hexagonMap!!.successShots + hexagonMap!!.failShots
            var progress = 0
            if (total != 0) {
                progress = 100 * hexagonMap!!.successShots / (total)
            }
            rootView!!.progressBar?.progress = progress
            rootView!!.textViewSuccessRate?.text = "$progress%"
            rootView!!.rootView!!.textViewTotalShot?.text = "$total"
            rootView!!.textViewSuccessFailShot?.text =
                String.format("%s/%s", hexagonMap!!.successShots, hexagonMap!!.failShots)
        }
    }
}
